<?php
namespace RubidotStarter;
/* Actions */
add_action( 'wp_enqueue_scripts', 'RubidotStarter\register_styles' );
add_action( 'wp_enqueue_scripts', 'RubidotStarter\enqueue_styles'  );


/* Functions */

function enqueue_styles(){
	wp_enqueue_style( 'styles' );
}
function register_styles(){
	$style_dir = get_template_directory_uri() . '/library/styles/css';
	wp_register_style(
		$handle = 'styles',
		$src    = "$style_dir/styles.css",
		$deps   = null,
		$ver    = null,
		$media  = 'all'
	);
}
